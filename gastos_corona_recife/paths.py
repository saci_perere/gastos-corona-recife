"""Coleção com os caminhos no sistema."""
#!/usr/bin/env python3
import os
from pathlib import Path
# Diretórios
#
#PATH_DIR = os.path.dirname(__file__)
PATH_DIR = Path(__file__).parent
PARENT_DIR = PATH_DIR.parent
PATH_DIR = str(PATH_DIR)
PARENT_DIR = str(PARENT_DIR)
#Path(__file__).parent.parent

#PATH_DIR = "/home/saci/hd/projetos/gastos-corona-recife/gastos_corona_recife/"
DATA_DIR = PATH_DIR + "/data/"

APP_DATA_DIR = PARENT_DIR + "/app_data/"
DATE_PATH = APP_DATA_DIR + "last_update.py"

# Arquivos
#
DISPENSAS = "DISPENSAS_portal_transparencia"
DESPESAS = "DESPESAS_portal_transparencia"

PARTIAL_DISPENSA_FILES = {
    "DISP_EXTENDIDO": DATA_DIR + DISPENSAS + "_EXTENDIDO.csv",
    "DISP_CONSOLIDADO": DATA_DIR + DISPENSAS + "_CONSOLIDADO.csv",
    "DISP_EMPENHO": DATA_DIR + DISPENSAS + "_EMPENHO.csv"
}
FILE_DISPENSAS = DATA_DIR + DISPENSAS + ".csv"
FILE_DISPENSAS_RAW = DATA_DIR + "RAW_" + DISPENSAS + ".csv"
# DISPENSAS = DATA_DIR + FILE_DISPENSAS


PORTAL_DESPESAS = {
    "PORTAL_FRONT": DATA_DIR + DESPESAS + ".csv",
    "PORTAL_DETAILS": DATA_DIR + DESPESAS + "_DETAILS.csv"

}
