from . import config

# recebia DF das dispensas!
# vai passar a receber dict, chave:orgao, values: lista com df's
# dict{orgao:[[df1,df2]...]...}

def limpeza_despesas(dfs):
    frame = pd.concat(dfs, ignore_index=True)
    if ('Unnamed: 0' in frame.columns):
        frame = frame.drop(['Unnamed: 0'], axis=1)
        ### Renomear as colunas e criar novas
    print(frame.columns)
    frame.columns = ['cpf_cnpj', 'credor_nome', 'empenho', 'ordem_bancaria',
                     'detalhes_ob', 'elemento_nome', 'subelemento_nome',
                     'modalidade', 'licitacao_tipo', 'referencia_legal', 'processo_numero',
                     'emissao_data', 'valor_empenhado', 'valor_liquidado', 'valor_pago',
                     'pagamento_data', 'valor_anulado', 'estorno_pago',
                     'estorno_liquidado', 'ultimo_movimento_data', 'orgao_uniadde',
                     'OU_nome', 'OU_codigo', 'total_detalhes', 'valor_anulado_detalhes',
                     'saldo_detalhes', 'itens', 'quantidade',
                     'valor_item', 'descricao', 'total_itens']
    frame['empenho_numero'] = frame.empenho.apply(lambda x: x.split('NE')[1][:5])
    frame['subempenho_numero'] = frame.empenho.apply(lambda x: x.split('NE')[1][5:])
    frame['empenho_ano'] = frame.empenho.apply(lambda x: x.split('NE')[0])
    ### Tratamento dos campos
    ##Drops
    frame = frame[frame.licitacao_tipo == 'DISPENSADO']
    frame = frame.drop(['empenho',
                        #'ordem_bancaria',
                        #'detalhes_ob',
                        'licitacao_tipo',
                        'processo_numero'], axis=1)
    ##Mudanças de tipos e replace
    barra = ['cpf_cnpj', 'modalidade', 'emissao_data', 'ultimo_movimento_data']
    for x in barra:
        frame[x] = frame[x].str.replace('\\','')
        datas = ['emissao_data', 'pagamento_data', 'ultimo_movimento_data']
    for x in datas:
        frame[x] = pd.to_datetime(frame[x], dayfirst=True)
        money = ['valor_empenhado', 'valor_liquidado', 'valor_pago',
                 'valor_anulado', 'estorno_pago', 'estorno_liquidado',
                 'total_detalhes', 'valor_anulado_detalhes', 'saldo_detalhes',
                 'valor_item','total_itens']
    for x in money:
        frame[x] = (frame[x].str.replace('.','').astype('float'))/100
        ### Resatar index e vai!
    return frame.reset_index()

def detalhes2df_coronagastos(orgao_dfs):
    #    print(orgao_dfs)
    dfs = []
    for x in orgao_dfs:
        print (type(x))
        for orgao in x.keys():
            print('\n\n<<<{}>>>\n\n'.format(orgao))
            codigo = config.nome_codigo[orgao.upper()]
            orgao_unidade = 'unidade' if ('.' in codigo) else 'orgao'
            cnpj_lista = []
            detalhes_horizontal = x[orgao][0].T
            detalhes_horizontal.columns = detalhes_horizontal.loc[0]
            detalhes_horizontal = detalhes_horizontal.drop(index=0)
            detalhes_horizontal.reset_index(drop=True, inplace=True)
            detalhes_saldo = x[orgao][1].iloc[-4:-1].reset_index(drop=True)
            detalhes_horizontal['orgao_unidade'] = orgao_unidade
            detalhes_horizontal['OU_nome'] = orgao
            detalhes_horizontal['OU_codigo'] = codigo
            detalhes_horizontal['total_detalhes'] = \
                detalhes_saldo['Total'].loc[0]
            detalhes_horizontal['valor_anulado_detalhes'] = \
                detalhes_saldo['Total'].loc[1]
            detalhes_horizontal['saldo_detalhes'] = \
                detalhes_saldo['Total'].loc[2]
            detalhes_horizontal['itens'] = False
            detalhes_horizontal['quantidade'] = None
            detalhes_horizontal['valor_item'] = None
            detalhes_horizontal['descricao'] = None
            detalhes_horizontal['total_itens'] = None
            detalhes_vertical = detalhes_horizontal.copy()
            detalhes_vertical['itens'] = True
            for celula in x[orgao][1].iloc[:-4].values:
                detalhes_vertical['quantidade'] = celula[0]
                detalhes_vertical['valor_item'] = celula[1]
                detalhes_vertical['descricao'] = celula[2]
                detalhes_vertical['total_itens'] = celula[3]
                detalhes_concat = pd.concat(
                    [detalhes_horizontal,detalhes_vertical],
                    ignore_index=True)
                cnpj_lista.append(detalhes_concat)
                cnpj_lista = pd.concat(cnpj_lista, ignore_index=True)
                #                    cnpj_lista.to_csv('csv/RAW'
                #                                      + functions.timestamping()
                #                                      + '_' + str(orgao) + '.csv')
                ### ??Verificar para que seria a função trata_consulta
                #dfs.append(trata_consulta(cnpj_lista))
        dfs.append(cnpj_lista)
    return(limpeza_despesas(dfs))
