import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import glob
import paths

import functions

def load():
#    li = []
#    all_files = glob.glob(str(paths.PLANILHAS_COVID) + "/*.xls*")
#    for filename in all_files:
#        df = pd.read_excel(filename, skiprows=6, head=6)
#        orgaoNome = filename.split('/')[-1:][0].split('-')[-1:][0][3:].split('.')[0].replace('_', ' ')
#        df = df.drop(df[df['Nº Dispensa'] == 'TOTAL'].index)
#        df = df.drop(df.filter(regex='^Unnamed', axis=1).axes[1], axis=1)
#        df.columns = ['dispensa', 'retificacao','orgao','objeto','vigencia','cnpj','fornecedor','valor']
#        df.orgao = orgaoNome
#        li.append(df)
#    df = pd.concat(li, ignore_index=True)
#    df.to_csv('../data/dispensas_COVID19.csv')
    df = pd.read_csv('../data/consolidado.csv')
    return df
