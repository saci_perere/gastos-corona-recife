#!/bin/python

"""Return [[tabela1,tabela2],...]
Para cada órgão retorna uma lista, cada elemento referente a um CNPJ,
contendo listas com dois dataframes, referente às tabelas horizontais e
verticais encontradas dentro do detalhamento de cada linha da resposta
à query utilizando payload_consult
Admite entrada enquanto órgão ou unidade. Separa os dois casos dentro da
inicialização da classe, baseado no dicionário contido no módulo paths.
<TODO>
- Receber qualquer query (não apenas cnpj * orgao)
- Validar entradas (utilizando dicionário em paths)
"""
import time
import sys
import requests
import pandas as pd
from bs4 import BeautifulSoup

import paths

requests.adapters.DEFAULT_RETRIES = 10

class Consults_details:

    def __init__(self, cnpj, orgao):

        self.payload_consult = { \
                                 'ano': '2020', \
                                 'mesInicial': '01', \
                                 'mesFinal': '07', \
                                 'cnpj': cnpj \
        }
        codigo = paths.nome_codigo[orgao.upper()]
        if ('.' in codigo):
            self.payload_consult['unidade'] = codigo
        else:
            self.payload_consult['orgao'] = codigo

    def load_details(self, a):
        self.payload_details = { \
                                 'ano' : a['ano'], \
                                 'mes' : a['mes'], \
                                 'unidade' : a['unidade'], \
                                 'nempenho' : a['empenho'], \
	                         'nsubempenho' : a['subempenho'], \
	                         'cpfcnpj' : a['id'], \
	                         'credor' : a['nome'], \
	                         'modalidade' : a['modalidade'], \
	                         'descsituacao' : a['desc_situacao'], \
	                         'tipolicitacao' : a['tipo_licitacao'], \
	                         'dataemissao' :a['dataemissao'], \
	                         'dthremissao' : a['dthremissao'], \
	                         'valorempenhado' : a['valor_empenhado'], \
	                         'valorliquidado' : a['valor_liquidado'], \
	                         'valorpago' : a['valor_pago'], \
	                         'dtultmov' :a['dtultmov'], \
	                         'valoranulado' : a['valor_anulado'], \
	                         'estornopago' : a['estorno_pago'], \
	                         'estornoliquidado' : a['estorno_liquidado'],  \
        }

    def get_consulta(self):
        url ='http://transparencia.recife.pe.gov.br/codigos/web/despesas/despesaDetalhadaCredorServerProcessing.php'
#        print (url)
#        print(self.payload_consult)
        r = requests.get( url,
                          params=self.payload_consult,
                          headers={'Connection':'close'},
                          timeout=30
        )
        ### Retorna estrutura do bs4 com a tabela referente a um empenho
        ### cada elemento é uma linha. Objeto Iteravel para recuperar caso a caso.
        return BeautifulSoup(r.text, features='lxml').find_all('a')

    def get_details(self):
        url = 'http://transparencia.recife.pe.gov.br/codigos/web/despesas/despesaDetalhadaCredor_ajaxEmpenho.php'
#        print(url)
#        print(self.payload_details)
        time.sleep(2)
        r = requests.get( url,
                          params = self.payload_details,
                          headers = {'Connection':'close'},
                          timeout=30
        )
        ### Return a Soup
        return BeautifulSoup(r.text, features='lxml')


def main(cnpj, orgao):
    pl = Consults_details(cnpj, orgao)
    consulta_table = pl.get_consulta()
#    return consulta_table
    dfs = []
    for x in consulta_table:
        pl.load_details(x.attrs)
        y = pl.get_details()
        ### y contém duas tabelas: uma horizontal e outra vertical
        try:
            dfs.append(pd.read_html(str(y)))
        except:
            print('##################')
            print(str(y))
            input()
    return dfs

if (__name__ == '__main__'):
    main(sys.argv[1:])
