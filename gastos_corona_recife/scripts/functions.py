import subprocess as sp
from datetime import datetime

def sha512sum (filename):
    return sp.run(['sha512sum', str(filename)], \
                  capture_output=True).stdout.decode().split(' ')[0]

def timestamping ():
    data = datetime.now()
    yy = str(data.year)
    mm = data.month
    dd = data.day
    dd = ''.join(['0', str(dd)]) if (dd < 10) else str(dd)
    mm = ''.join(['0', str(mm)]) if (mm < 10) else str(mm)
    return yy + '-' + mm + '-' + dd + '_'
