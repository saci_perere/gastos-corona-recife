#!/bin/python3

"""Return [Updated files]
baixa os arquivos disponíveis no sítio da prefeitura do Recife
com os gastos dispensados de licitação devida à pandemia do
COVID19.
Salva os arquivos em ../data
Confere o SUM512SHA dos arquivos para saber se houve modificação
dos arquivos baixado em relação aos pré-existentes

<TODO>
Só pega o primeiro LI das secretarias, etc...
não pega o PDF, etc...
incrementar...
"""

import re
import urllib
import logging
import subprocess as sp
from datetime import datetime
from pathlib import Path

import paths
import functions

def main():
    logging.basicConfig( \
                         filename='../log/log.txt', \
                         filemode='w', \
                         level=logging.INFO, \
                         format='%(asctime)s - %(levelname)s - %(message)s' \
    )
    ### Encontro todas as listas
    for x in paths.soup.find_all('li'):
        if (x.span != None):
            a = re.compile(paths.pt).match(str(x.span))
            ### Encontra as listas que têm a tag SPAN e
            ### casam com o padrão descrito em paths.pt
            if (a != None):
                ### Encontra todas as listas internas
                ### ao tópico de interesse (path.pt)
                for y in x.find_all('li'):
                    unit_name = str(y.span)[6:-7].replace(' ', '_')
                    logging.info(unit_name)
                    if (y.li != None):
                        try:
                            ### Pula para o primeiro link (TODO)
                            pre_url = y.li.a['href']
                            ### Codifica para o formato da url;
                            ### tira espaços, etc
                            url = 'http://' \
                                + urllib.request.pathname2url(pre_url[7:])
                            filename = unit_name + '.' \
                                + pre_url.split('.')[-1:][0]
                            ### AAAA/MM/DD
                            ts = functions.timestamping()
                            new_file = str(paths.TMPDATA) + '/' + ts + filename
                            cmd = 'curl ' + url + \
                                ' > ' + new_file
                            ### Baixa o arquivo na pasta tmp
#                            print (cmd)
                            sp.Popen(cmd, shell=True).wait()
                            logging.info('Download concluído\nurl: ' + url)
                            if (Path(new_file).is_file()):
                                logging.info('Arquivo criado em: ' + new_file)
                            else:
                                logging.erro( \
                                    'Arquivo não foi criado: ' + new_file)
                        except:
                            logging.erro('CURL não realizou o Download')
                            raise ValueError('CURL não realizou o Download')

                        update(new_file, unit_name, ts, filename)

def update (new_file, unit_name, ts, filename):
    #######
    ### Captura o nome do arquivo anterior
    old_file = get_oldfile(unit_name)
    ### Verifica se há modificação no hash:SHA512SUM
    if (update_table(old_file, new_file)):
        ### Move arquivo antigo para data/old
        ### (repositório das mudanças)
        if (not old_file is None):
            sp.run([ 'mv',
                     str(paths.DATA) + '/' + old_file,
                     str(paths.DATA.joinpath('old'))
                     + '/' + old_file])
            ### Move o arquivo atualizado para a pasta data
            sp.run([ 'mv',
                     new_file,
                     str(paths.DATA) + '/'+ ts+filename])
        else:
            logging.info('Sem atualização')
            ### Exclui o arquivo novo;
            ### não houveram modificações
            logging.info('Arquivo para excluir em: ' + new_file)
            sp.run(['rm', new_file])

##########################
### Funções auxiliares ###
##########################

def update_table(old_file, new_file):
    """
    True  <- a tabela encontrada é diferente da atual
    False <- a mesma tabela
    """
    if (old_file is None):
        return True
    old_file = str(paths.DATA) + '/' +old_file
    s512_old = functions.sha512sum(old_file)
    s512_new = functions.sha512sum(new_file)

    logging.info('Arquivo atual:')
    logging.info(old_file)
    logging.info('sha512sum:')
    logging.info(s512_old)
    logging.info('Arquivo novo:')
    logging.info(new_file)
    logging.info('sha512sum:')
    logging.info(s512_new)

    return False if \
        (s512_old == s512_new) \
        else True

def get_oldfile(unit_name):
    ls_output = sp.run([ \
                         'ls', \
                         str(paths.DATA)], \
                       capture_output=True).stdout.decode().split('\n')
    for x in ls_output:
        if (unit_name in x):
            return x
    return None

if (__name__ == '__main__'):
    main()
