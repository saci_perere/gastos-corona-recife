# Execução do pacote por linha de comando
# from gastos_corona_recife import Data
# data = Data()
# data.update_databases()
# data.df2csv()
#

from gastos_corona_recife import dispensas_etl, empenhos_to_app, paths
from datetime import datetime

def main():
    dispensas_etl.dispensas2empenho(scrap=True, consolidadas=False)
    data = empenhos_to_app.Data_despesas()
    data.etl_portal_despesas()
    data.process_to_app()
    d = datetime.now().strftime("%d/%m/%y")
    with open(paths.DATE_PATH, 'w') as f:
        f.write(f'date = "{d}"')

if __name__ == "__main__":
    main()
