#!/usr/bin/env python3
# import sys
# import pathlib
import os

import pandas as pd
from unidecode import unidecode

from gastos_corona_recife.adapters import (
    chave_dispensas_portal_transparencia, dispensas_portal_transparencia)
from gastos_corona_recife.schemas import SCHEMA_DISPENSAS
from gastos_corona_recife import (paths, transform)


def dispensas_full(df: pd.DataFrame) -> pd.DataFrame:
    """Retorna as informações de dispensas com o empenho."""
    df = _normalize_columns_names(df)
    df = _clean_columns(df)
    df = _remove_duplicated_column_name(df)
    df = _adjust_types(df)
    return df


def _just_type(df: pd.DataFrame, t: str) -> pd.DataFrame:
    df = df.astype(t, errors='ignore')
    return df


def _adjust_int(df: pd.DataFrame, t: str) -> pd.DataFrame:
    for col in SCHEMA_DISPENSAS[t]:
        df[col] = pd.to_numeric(df[col], errors='coerce')
    df = df.astype(t, errors='ignore')
    return df



def _adjust_floats(df: pd.DataFrame, t: str) -> pd.DataFrame:
    df = df.apply(lambda x: x.str.replace("R\$ ",""))
    dot_comma = df.apply(lambda x: '.'
                         if len(x[x.str[-3] == ',']) == 0
                         else ',')
    assert all(e == ',' for e in dot_comma)
    df = df.apply(lambda x: x.str.replace(".",""))
    df = df.apply(lambda x: x.str.replace(",","."))
    df = df.astype(t)
    return df


def _adjust_date(df: pd.DataFrame, t: str) -> pd.DataFrame:
    for col in SCHEMA_DISPENSAS[t]:
        df[col] = pd.to_datetime(
            df[col],
            format="%d/%m/%Y",
            errors="coerce")
    return df


def _adjust_types(df: pd.DataFrame) -> pd.DataFrame:
    """Modifica o dataframe de acordo com o schema."""
    dict_funcs = {
        'float': _adjust_floats,
        'datetime64[ns]': _adjust_date,
        'int': _adjust_int,
        'str': _just_type
    }
    for t in SCHEMA_DISPENSAS:
        df[SCHEMA_DISPENSAS[t]] = dict_funcs[t](df[SCHEMA_DISPENSAS[t]], t)
    return df


def _clean_columns(df: pd.DataFrame) -> pd.DataFrame:
    """Remove colunas a partir de critéiros definidos no corpo."""
    # Todas as linhas vazias
    df = df.dropna(axis=1, how='all')
    return df

def _remove_duplicated_column_name(df):
    df, max = _suffix_in_repeated_cols(df)
    dup_cols = df.columns[df.columns.str.endswith('_0')]
    df[dup_cols.str.replace("_0","")] = \
        df[df.columns[df.columns.str.endswith('_0')]]
    for i in range(max):
        df = df.drop(df.columns[df.columns.str.endswith(f'_{i}')], axis=1)
    return df


def _suffix_in_repeated_cols(df: pd.DataFrame) -> pd.DataFrame:
    """Renomeia colunas com nome idêntico."""
    cols = df.columns
    cols_dup = df.columns[df.columns.duplicated()]
    max_duplicated = 1
    for col in cols_dup:
        duplicated = list(filter(lambda x: col == x, cols))
        assert (len(duplicated) > 1)
        max_duplicated = _greater(max_duplicated, len(duplicated))
        replace = {col: [col + "_" + str(n)
                         for n in range(len(duplicated))]}
        df = df.rename(columns=lambda c: replace[c].pop(0)
                       if c in replace.keys() else c)
    return df, max_duplicated


def _greater(a,b):
    """Getorna o maior valor"""
    return a if a > b else b


def _normalize_columns_names(df: pd.DataFrame) -> pd.DataFrame:
    """Normaliza os nomes das colunas."""
    cols = list(map(normalize_str, df.columns))
    df.columns = cols
    return df


def normalize_str(string: str) -> str:
    """Deixas as strings no formato [a-z0-9_]* ."""
    new_string = unidecode(string.lower()
                           .replace(' ', '_')
                           .replace(":", ""))
    return new_string


def get_from_html_tag(df: pd.DataFrame) -> pd.DataFrame:
    """Processamento do dado bruto."""
    df['chave_lupa'] = df['chave_lupa'].str.split(' ').apply(
        lambda x: x[3].split('=')[1].replace('\'', ''))
    df['num_processo'] = df['num_processo'].str.split('>').apply(
        lambda x: x[1][:-3] if len(x) > 1 else x[0])
    df['num_contrato'] = df['num_contrato'].str.split('>').apply(
        lambda x: x[1][:-3] if len(x) > 1 else x[0])
    return df
