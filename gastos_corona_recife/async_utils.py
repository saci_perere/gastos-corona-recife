#!/usr/bin/env ipython
import aiohttp
import asyncio
from gastos_corona_recife import paths, utils

class Async_requests:

    def __init__(self, target, payloads):
        self._rest_payloads = []
        self.url = 'http://transparencia.recife.pe.gov.br/codigos/web/despesas/despesaDetalhadaCredorServerProcessing.php'
        self.url_details = 'http://transparencia.recife.pe.gov.br/codigos/web/despesas/despesaDetalhadaCredor_ajaxEmpenho.php'

        if (target == "despesas"):
            self.response = self.loop_func(self.fetch_despesas, payloads)

        elif (target == "detalhes"):
            responses_details = self.loop_func(self.fetch_details, payloads)
            flat_response_details = {k: v for dict_response in responses_details for k, v in dict_response.items()}
            self.response = flat_response_details
        else:
            raise Exception("Alvo sem receita definida!")

    def loop_func(self, func, parameters):
        done = False
        responses = []

        while (not done):
            loop = asyncio.get_event_loop()
            future = asyncio.ensure_future(self.run(func, parameters))
            actual_responses = loop.run_until_complete(future)
            responses.extend(actual_responses)

            if(len(self._rest_payloads)):
                parameters = self._rest_payloads
            else:
                done = True
                self._rest_payloads = []

        return responses

    async def fetch_despesas(self, session, pl):
        try:
            async with session.get(self.url, params=pl) as response:
                return await response.json(content_type=None)
        except:
            self._rest_payloads.append(pl)


    async def fetch_details(self, session, pl):
        try:
            async with session.get(self.url_details, params=pl) as response:
                return {pl.values(): await response.text()}
        except:
            self._rest_payloads.append(pl)

    async def run(self, fetch, pls):
        connector = aiohttp.TCPConnector(force_close=True)
        session_timeout = aiohttp.ClientTimeout(total=None, sock_connect=300, sock_read=300)
        tasks = []
        async with aiohttp.ClientSession(connector=connector, timeout=session_timeout) as session:
            for pl in pls:
                task = asyncio.ensure_future(fetch(session, pl))
                tasks.append(task)
            responses = await asyncio.gather(*tasks)
            return responses
