from pathlib import Path

import requests
from bs4 import BeautifulSoup

source_dispensas = 'data/dispensas.csv'
source_empenhos = 'data/empenhos.csv'

BASE = Path('..')
TMPDATA = BASE / 'tmp' / 'data'
DATA = BASE / 'data'
ANALISE = DATA / 'analise'
CSV_DESPESAS = DATA / 'despesas'
PLANILHAS_COVID = DATA / 'planilhas_COVID19'

pt = '<span>Processos de aquisições de bens e serviços com base na Lei nº 13.979/2020</span>'
#soup = BeautifulSoup(\
#                     requests.get('http://transparencia.recife.pe.gov.br/codigos/web/estaticos/estaticos.php?nat=COV#filho').text, \
#                     features="lxml")
#

# Endereços WEB

portal_transparencia = 'http://transparencia.recife.pe.gov.br/codigos/web/despesas/despesaDetalhadaCredorServerProcessing.php'
portal_transparencia_detalhes = 'http://transparencia.recife.pe.gov.br/codigos/web/despesas/despesaDetalhadaCredor_ajaxEmpenho.php'
portal_transparencia_dispensas = 'http://transparencia.recife.pe.gov.br/codigos/web/estaticos/estaticos.php?nat=COV#filho'

nome_codigo  = {
    'SECRETARIA DE EDUCAÇÃO - ADMINISTRAÇÃO DIRETA': '14.01',
    'SECRETARIA DE INFRAESTRUTURA - ADMINISTRAÇÃO DIRETA': '20.01',
    'GABINETE DE PROJETOS ESPECIAIS - ADMINISTRAÇÃO DIRETA': '26.01',
    'SECRETARIA DE DESENV SOCIAL, JUVENTUDE, POLÍT. SOBRE DROGAS E DIR. HUMANOS - ADMINISTRAÇÃO DIRETA' : '29.1',
    'FUNDO MUNICIPAL DOS DIREITOS DA PESSOA IDOSA': '59.5',
    'FUNDO MUNICIPAL DE SAÚDE - FMS': '48.01',
    'FUNDO MUNICIPAL DE ASSISTÊNCIA SOCIAL - FMAS': '59.01',
    'SECRETARIA DE GOVERNO E PARTICIPAÇÃO SOCIAL - ADMINISTRAÇÃO DIRETA' : '25.01'
}

api_url_dispensas = {
    'FMS':{
        '8666':{'ea7c0c62-995a-4917-b267-59a2ec67900e'},
        '13979':{'c0db4f96-97f7-4bdf-a45b-1d2140b52b87'}
    },
    'SEGOV':{
        '8666':{''},
        '13979':{''}
    },
    '':{
        '8666':{''},
        '13979':{''}
    },
    '':{
        '8666':{''},
        '13979':{''}
    },
    '':{
        '8666':{''},
        '13979':{''}
    },
    '':{
        '8666':{''},
        '13979':{''}
    },
    '':{
        '8666':{''},
        '13979':{''}
    },
}
