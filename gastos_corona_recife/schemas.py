"""Informações relativas aos nomes e tipo dos schemas."""
#!/usr/bin/env python3

# Colunas dispensa (fase 1)
INT_COLS = ['num_processo', 'qtd_item', 'ano']
FLOAT_COLS = ['valor_total_processo', 'valor_unitario_item', 'valor_total_item', 'valor_fornecedor']
DATE_COLS = ['data_vigencia']

# Colunas finais (join(fase 1, fase 2))
#
SCHEMA_DISPENSAS = {
    'datetime64[ns]': [
        'data_emissao', 'data_pagamento',
        'data_anulacao', 'data_de_publicacao_do_dom',
        'data_de_vigencia'],
    'int':[
        'numero_empenho', 'numero_do_processo', 'id'],
    'float': [
        'valor_empenhado', 'valor_pago',
        'valor_anulado', 'valor_do_fornecedor',
        'valor_total_do_processo'],
}

SCHEMA_DISPENSAS_EXT = {
    'datetime64[ns]': [
        'data_emissao', 'data_pagamento',
        'data_anulacao', 'data_de_publicacao_do_dom',
        'data_de_vigencia'],
    'int':[
        'numero_empenho', 'numero_do_processo', 'id', 'qtd_item'],
    'float': [
        'valor_empenhado', 'valor_pago',
        'valor_anulado', 'valor_do_fornecedor',
        'valor_total_do_processo', 'valor_unitario_item',
        'valor_total_item'],
}
