# Exportado do notebook! =P
#
import pandas as pd
import requests

from datetime import datetime
import numpy as np

from gastos_corona_recife import paths, utils
from gastos_corona_recife.async_utils import Async_requests
# Definições úteis

class Data_despesas:

    def __init__(self, consolidadas=False):
        self.url = 'http://transparencia.recife.pe.gov.br/codigos/web/despesas/despesaDetalhadaCredorServerProcessing.php'
        self.url_details = 'http://transparencia.recife.pe.gov.br/codigos/web/despesas/despesaDetalhadaCredor_ajaxEmpenho.php'
        self.df = pd.read_csv(paths.FILE_DISPENSAS)
        self.dfE = pd.read_csv(paths.PARTIAL_DISPENSA_FILES["DISP_EXTENDIDO"])
        self.ANOS = [2020, 2021]
        self.empenhos_unicos = self.df['numero_empenho'].unique() # ne
        self.all_cnpjs = self.df['cpf_cpnj_fornecedor'].unique()

    def etl_portal_despesas(self):
        self.df_init = self.initial_clean(self.df)
        payloads = self.generate_payload_from_dispensas(self.df_init)
        #payloads = payloads[:10]
        # Async Request: portal da transparência -> despesas
        async_request_despesas = Async_requests("despesas", payloads)
        response_portal = async_request_despesas.response
        (df_portal, payload_details) = self.process_request_despesas(response_portal)
        df_portal.to_csv(paths.PORTAL_DESPESAS["PORTAL_FRONT"])
        # Request: portal da transparência -> despesas -> detalhes
        async_request_detalhes = Async_requests("detalhes", payload_details)
        responses_details = async_request_detalhes.response
        self.df_details = utils.process_details(responses_details)
        self.df_details = self.filter_covid_empenhos(self.df_details)
        self.df_details.to_csv(paths.PORTAL_DESPESAS["PORTAL_DETAILS"], index=False)

    def initial_clean(self, df):
        df = utils.string_cols_to_lower(df)
        df['numero_empenho'] = df['numero_empenho'].fillna(0)
        # Split entidade governamental em [numero, nome, complemento]
        df[['num_entidade', 'nome_entidade', 'compl_entidade']] = \
            df.unidade.str.lower().str.split(' - ', n=0, expand=True)
        df['num_entidade'] = df['num_entidade'].str.replace('.','.0')
        cols = [col for _, cols in utils.group_cols.items() for col in cols]
        df = df[cols]
        return df

    def generate_payload_from_dispensas(self, df, cols=['cpf_cpnj_fornecedor', 'num_entidade', 'ano', 'numero_empenho']) -> list:
        df_to_request = df.groupby(cols).size().reset_index().drop(columns=0)
        pls = [{'cnpj': cnpj, 'unidade': unidade, 'ano': ano, 'empenho': int(empenho)}
               for cnpj, unidade, ano, empenho in df_to_request.values]
        return pls

    def process_request_despesas(self, responses):
        ## Return a list of dict, each one with a list of payloads
        ## dicts retains the key of request for future use, specifically "num_entidade"
        all_payloads = [utils.response_to_payload_detalhes(resp)
                        for resp in responses
                        if resp['iTotalRecords'] > 0]

        all_payloads_flat = utils.flat_list(all_payloads)
        df_list = utils.payloads_to_df_list(all_payloads_flat)
        df = pd.concat(df_list)
        return (df, all_payloads_flat)

    def filter_covid_empenhos(self, df):
        _df = pd.read_csv(paths.FILE_DISPENSAS)
        _df['n_und'] = _df['unidade'].apply(lambda x: x.split(' - ')[0]).str.replace('.','.0')
        emp = _df.groupby(['n_und', 'numero_empenho','cpf_cpnj_fornecedor']).size().reset_index()
        df['covid'] = False

        for _, r in emp.iterrows():
            df['covid'] = np.where(
                ((df['unidade_PLR'] == r['n_und'])
                 & (df['empenho_numero'] == r['numero_empenho'])
                 & (df['CPF/CNPJ:'] == r['cpf_cpnj_fornecedor'])),
                True,
                df['covid']
            )

        return df[df['covid'] == True]


    def process_to_app(self):
        flots = ['Valor Empenhado:', 'Valor Liquidado:', 'Valor Pago:', 'Valor Anulado:', 'Estorno Pago:', 'Estorno Liquidado:']
        df = utils.to_float(self.df_details, flots)
        num_nome_ent = self.df_init.groupby(['num_entidade','nome_entidade']).size().reset_index().drop(columns=0)

        df['nome_entidade'] = ''
        for num_entidade in df.unidade_PLR.unique():
            df['nome_entidade'] = np.where(
                df['unidade_PLR'] == num_entidade,
                num_nome_ent[num_nome_ent['num_entidade'] == num_entidade]['nome_entidade'].unique()[0],
                df['nome_entidade'])
        print(df.columns)
        df.rename(columns = utils.rename_dict_to_app, inplace=True)
        print(df.columns)
        #######
        df['empenho_numero'] = df['No Empenho:'].apply(lambda x: x.split('NE')[1][:5]).astype('int')
        df['subempenho_numero'] = df['No Empenho:'].apply(lambda x: x.split('NE')[1][5:]).astype('int')
        df['empenho_ano'] = df['No Empenho:'].apply(lambda x: x.split('NE')[0]).astype('int')

        df2 = df[df.itens==True]
        df = df[df.itens==False].copy()

        df['objetos'] = ""
        print(df.dtypes)
        for org in df["OU_nome"].unique():
            for emp in df[df["OU_nome"] == org]["empenho_numero"].unique():
                obj = df2[(df2["OU_nome"] == org) & (df2["empenho_numero"] == emp)].descricao.unique()
                join_descricoes = "< %s >" % ' >; < '.join(obj)
                #df = utils.objetos_empenhos(df, org, emp, join_descricoes)
                df['objetos'] = np.where(
                    (df['OU_nome'] == org)
                    & (df['empenho_numero'] == emp),
                    join_descricoes, df['objetos'])

        df['subelemento_nome'] = df['subelemento_nome'].apply(utils.return_subelem)
        df["ultimo_movimento_data"] = df["ultimo_movimento_data"].astype('datetime64[ns]')
        df['mes'] = df["ultimo_movimento_data"].apply(lambda x: x.month)

        cc = ['cpf_cnpj', 'credor_nome', 'elemento_nome', 'subelemento_nome',
              'referencia_legal','emissao_data','valor_empenhado','valor_liquidado',
              'valor_pago','valor_anulado','estorno_pago','estorno_liquidado','ultimo_movimento_data',
              'OU_nome','empenho_numero','subempenho_numero','objetos','mes','grupo']

        df["grupo"] = ""
        for i, r in df.iterrows():
            for grupo, value in utils.gdict.items():
                if (r.subelemento_nome in value):
                    df['grupo'] = np.where(
                        (df["cpf_cnpj"] == r.cpf_cnpj)
                        & (df["OU_nome"] == r.OU_nome)
                        & (df["empenho_numero"] == r.empenho_numero),
                grupo, df["grupo"])

        df = df[cc]

        df = df[(df.estorno_pago.isna()) | (df.estorno_pago == 0)]

        x = df.empenho_numero.value_counts()
        emp_unicos = x[x==1].index
        y = df[(df.empenho_numero.isin(emp_unicos)) & (df.valor_anulado == df.valor_empenhado)]
        empenhos_anulados = y.empenho_numero.unique()
        df = df[ ~(df.empenho_numero.isin(empenhos_anulados))]
        for z in df.OU_nome.unique():
            print(z)
            x = df[df.OU_nome == z].groupby('empenho_numero').valor_pago.sum()
            sem_pagos = x[x==0].index
            df = df[ ~(df.empenho_numero.isin(sem_pagos))]
        ###
        x = df[[
            'cpf_cnpj', 'credor_nome', 'OU_nome',
            'referencia_legal', 'subelemento_nome', 'grupo',
            'ultimo_movimento_data', 'empenho_numero', 'valor_pago',
            'mes', 'objetos']]
        x.columns = [
            'CNPJ', 'Fornecedor', 'Órgão',
            'Referência Legal', 'Subelemento', 'Grupo',
            #'Elemento',
            'Data Pagamento', 'Número Empenho', 'Valor pago',
            #'Total Detalhes',
            #'Número Subempenho',
            'Mês', 'Objetos Empenhados']

        x.to_csv(paths.APP_DATA_DIR + 'dtvz_pagamentos_data.csv', index=False)
        ###
        df_p = x
        fornecedores = df_p.groupby(['Fornecedor'])['Valor pago'].sum().nlargest(10).index
        df10mais = df_p[(df_p['Fornecedor'].isin(fornecedores))]
        df10mais = df10mais.groupby(['Fornecedor', 'Grupo'])['Valor pago'].sum().reset_index()
        df10mais = df10mais[df10mais['Valor pago'] != 0]
        df10mais2 = df_p[(df_p['Fornecedor'].isin(fornecedores))]
        df10mais2 = df10mais.groupby(['Fornecedor'])['Valor pago'].sum().reset_index()
        df10mais3 = df10mais.groupby('Fornecedor')['Grupo'].apply(lambda x: "{%s}" % ', '.join(x)).reset_index()
        df10mais = pd.DataFrame({'Fornecedor':df10mais2.Fornecedor,'Grupo':df10mais3.Grupo, 'Valor pago': df10mais2['Valor pago']})
        df10mais4 = df10mais.sort_values('Valor pago', ascending=False)
        def format(x):
            if (x > 1000000):
                x=x/1000000
                y = 'Milhões'
            else:
                x=x/1000
                y = 'Mil'
            x = "R${:.2f} ".format(x) + y
            x = x.replace('.',',')
            return x
        df10mais4 = df10mais4.sort_values('Valor pago', ascending=False)
        df10mais4['Valor pago'] = df10mais4['Valor pago'].apply(format)
        df10mais4.to_csv(paths.APP_DATA_DIR + 'df_10mais.csv', index=False)
        ###
        df_c = self.dfE.copy()
        df_c['valor_fornecedor'] = df_c['valor_fornecedor'].str.replace('.','')
        df_c['valor_fornecedor'] = df_c['valor_fornecedor'].str.replace(',','.')
        df_c['valor_fornecedor'] = df_c['valor_fornecedor'].astype('float')
        df_c['unidade'] = df_c['unidade'].str.split(
            ' - ',expand=True )[1].str.lower()
        df_c = df_c[['unidade', 'valor_fornecedor']]
        df_c.columns = ['Órgão', 'Valor do fornecedor']
        ###
        df_c = df_c.groupby('Órgão').sum().reset_index()
        df_c.to_csv(paths.APP_DATA_DIR + 'df_covid.csv')
        ###
        df_o = x.groupby('Órgão')['Valor pago'].sum().reset_index()
        df_c['valor_pago'] = 0

        def valor_agg(orgao, valor):
            df_c['valor_pago'] = np.where((df_c['Órgão'] == orgao), valor, df_c.valor_pago)

        for i,r in df_c.iterrows():
            try:
                valor_agg(r['Órgão'], df_o[df_o['Órgão'] == r['Órgão']]['Valor pago'].iloc[0] )
            except:
                print('Não há pagamentos para {}'.format(r['Órgão']))

        df_c['orgao'] = df_c['Órgão']
        df_c['valor_dispensado'] = df_c['Valor do fornecedor']
        df_c.to_csv(paths.APP_DATA_DIR + 'df_orgaos.csv')
        ###
        t = df.groupby(['credor_nome','subelemento_nome',
                        'OU_nome', 'referencia_legal','empenho_numero'])[[
                            'valor_empenhado','valor_liquidado','valor_pago',
                            'valor_anulado','estorno_pago','estorno_liquidado'
                        ]].sum().reset_index()
        t.sort_values('valor_empenhado',ascending=False)
        t['empenhado'] = t.valor_empenhado-t.valor_anulado
        t['liquidado'] = t.valor_liquidado-t.estorno_liquidado
        t['pago'] = t.valor_pago-t.estorno_pago
        t.drop([
            'valor_empenhado','valor_liquidado','valor_pago',
            'valor_anulado','estorno_pago','estorno_liquidado'
        ], axis=1, inplace=True)
        ###
        fornecedores = df.groupby(['credor_nome'])['valor_pago'].sum().nlargest(10).index
        df10mais = df[(df['credor_nome'].isin(fornecedores))]
        df10mais = df10mais.groupby(['credor_nome', 'OU_nome'])['valor_pago'].sum().reset_index()
        df10mais = df10mais[df10mais['valor_pago'] != 0]
        nodes = [*df10mais.credor_nome.unique(), *df10mais.OU_nome.unique()]
        source = []
        target = []
        for index, row in df10mais.iterrows():
            source.append(nodes.index(row.OU_nome))
            target.append(nodes.index(row.credor_nome))
        df10mais['source'] = source
        df10mais['target'] = target
        df10mais.to_csv(paths.APP_DATA_DIR + 'orgao_fornecedor_flow.csv')
