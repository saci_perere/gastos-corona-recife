import pandas as pd

group_cols = {
    'empenho': [
        'ano', 'num_entidade', 'nome_entidade',
        'compl_entidade', 'num_processo', 'num_contrato',
        'numero_empenho', 'id', 'modalidade',
        'situacao', 'objeto'],
    # 'numero_do_processo', unidade
    'datas': [
        'data_emissao', 'data_pagamento', 'data_anulacao',
        'data_de_publicacao_do_dom', 'data_de_vigencia'],
    # , 'data_vigencia'
    'fornecedor': [
         'nome_fornecedor', 'cpf_cpnj_fornecedor', 'local_de_execucao'],
    # 'nome_do_fornecedor','cpf/cnpj_do_fornecedor',
    'valores': [
        'valor_empenhado', 'valor_pago', 'valor_anulado',
        'valor_do_fornecedor', 'valor_total_do_processo'],
    # 'valor_fornecedor', 'valor_total_processo'
    'portal': ['chave_lupa', 'type_request'],

}

rename_dict_to_app = {
    'Valor Empenhado:' : 'valor_empenhado',
    'Valor Liquidado:' : 'valor_liquidado',
    'Valor Pago:' : 'valor_pago',
    'Valor Anulado:' : 'valor_anulado',
    'Estorno Pago:' : 'estorno_pago',
    'Estorno Liquidado:' : 'estorno_liquidado',
    'Data Último Movimento:' : 'ultimo_movimento_data',
    'nome_entidade' : 'OU_nome',
    'Sem Agrupamento' : 'grupo',
    'CPF/CNPJ:' : 'cpf_cnpj',
    'Nome Credor:' : 'credor_nome',
    'Elemento Despesa:' : 'elemento_nome',
    'SubElemento Despesa:' : 'subelemento_nome',
    'Referência Legal:' : 'referencia_legal',
    'Data Emissão:' : 'emissao_data',
    'Descrição' : 'descricao'
}

gdict={
    'COMUNICAÇÃO':[
        'SERVIÇOS DE COMUNICAÇÃO EM GERAL',
        'SERVIÇOS GRÁFICOS, CONFECÇÃO DE CARIMBOS E PLAQUETAS EM GERAL',
    ],
    'GESTÃO HOSPITALAR':[
        'AUXILIO PARA INSTITUIÇÕES PRIVADAS',
        'SUBVENÇÕES SOCIAIS A INSTITUIÇÕES PRIVADAS SEM FINS LUCRATIVOS',
        'SERVIÇOS MÉDICO-HOSPITALARES, ODONTOLÓGICOS E LABORATORIAIS',
    ],
    'LOGÍSTICA E TRANSPORTE':[
        'SERVIÇOS DE LOGÍSTICA, DISTRIBUIÇÃO E GESTÃO DE TRANSPORTE DE CARGAS.'
    ],
    'MATERIAIS E ANÁLISES LABORATORIAIS':[
        'MATERIAL LABORATORIAL'
    ],
    'MATERIAIS HOSPITALARES E EPIS':[
        'MATERIAL HOSPITALAR',
        'MATERIAL ODONTOLÓGICO',
        'MATERIAL DE PROTEÇÃO E SEGURANÇA'
    ],
    'MEDICAMENTOS':[
        'MATERIAL FARMACOLÓGICO',
        'MEDICAMENTOS'
    ],
    'MOBILIÁRIOS E EQUIPAMENTOS HOSPITALARES':[
        'SERVIÇOS TÉCNICOS PROFISSIONAIS',
        'MOBILIÁRIO EM GERAL',
        'ASSESSORIA E CONSULTORIA TÉCNICA - PESSOA JURÍDICA',
        'APARassertELHOS DE MEDIÇÃO E ORIENTAÇÃO',
        'SERVIÇOS DE CONFECÇÃO DE BENS MÓVEIS DE QUALQUER NATUREZA',
        'MATERIAL PARA MANUTENÇÃO DE BENS MÓVEIS',
        'MATERIAL DE CAMA, MESA E BANHO',
        'APARELHOS, EQUIPAMENTOS, UTENSÍLIOS MÉDICOS, ODONTOLÓGICOS, LABORATORIAIS E HOSPITALAR'
    ],
    'OBRAS E INSTALAÇÕES':[
        'INSTALAÇÕES DE OBRAS',
        'CESSÃO DE USO DE TENDAS, GALPÕES, PAVILHÕES, EQUIPAMENTOS E ASSEMELHADOS',
        'LOCAÇÃO DE MÁQUINAS E EQUIPAMENTOS',
        'MANUTENÇÃO E CONSERVAÇÃO DE BENS IMÓVEIS',
    ],
    'ALIMENTAÇÃO':[
        'GÊNEROS DE ALIMENTAÇÃO',
        'MATERIAL DE ACONDICIONAMENTO E EMBALAGEM',
        'FORNECIMENTO DE CESTAS BÁSICAS PARA FAMÍLIAS CARENTES',
        'FORNECIMENTO DE ALIMENTAÇÃO',
    ],
    'SERVIÇO FUNERÁRIO':[
        'SERVIÇOS FUNERÁRIOS PARA FAMÍLIAS CARENTES'],
    'MATERIAL DE LIMPEZA':[
        'MATERIAL DE LIMPEZA E PRODUTOS DE HIGIENIZAÇÃO - MATERIAIS E UTENSÍLIOS DE LIMPEZA',
        'MATERIAL DE LIMPEZA E PRODUTOS DE HIGIENIZAÇÃO - MATERIAIS E UTENSÍLIOS DE LIMPEZA.'
    ],
    'EXPOSIÇÕES':[
        'EXPOSIÇÕES, CONGRESSOS E CONFERÊNCIAS'
    ],
    'SINALIZAÇÃO':[
        'MATERIAL PARA SINALIZAÇÃO VISUAL E AFINS'
    ],
    'VIGILÂNCIA':[
        'VIGILÂNCIA OSTENSIVA'
    ],
    'OUTROS':[
        'GASSE MEDICINAIS',
        'MÁQUINAS E EQUIPAMENTOS AGRICOLAS E RODOVIÁRIOS'
    ]
}


def string_cols_to_lower(df):
    # To lower, Strings!
    types = df.dtypes
    string_cols = types[types == 'object'].index
    for s in string_cols:
        df[s] = df[s].str.lower()
    return df

def load_payload_detalhes(a):
    return {
        'ano': a['ano'],
        'mes': a['mes'],
        'unidade': a['unidade'],
        'nempenho': a['empenho'],
        'nsubempenho': a['subempenho'],
        'cpfcnpj': a['id'],
        'credor': a['nome'],
        'modalidade': a['modalidade'],
        'descsituacao': a['desc_situacao'],
        'tipolicitacao': a['tipo_licitacao'],
        'dataemissao':a['dataemissao'],
        'dthremissao': a['dthremissao'],
        'valorempenhado': a['valor_empenhado'],
        'valorliquidado': a['valor_liquidado'],
        'valorpago': a['valor_pago'],
        'dtultmov': a['dtultmov'],
        'valoranulado': a['valor_anulado'],
        'estornopago': a['estorno_pago'],
        'estornoliquidado': a['estorno_liquidado'],
    }

def flat_list(list_of_lists):
    list_of_inner_itens = [item for list_of_itens in list_of_lists for item in list_of_itens]
    return list_of_inner_itens

def payloads_to_df_list(pls):
    df_list = [pd.DataFrame({k: [v]  for k, v in payload.items() }) for payload in pls]
    return df_list

def data_to_payload(data):
    lst = data[0].split('\r\n')
    lst[0] = lst[0].split(";' ")[-1]
    lst = [l.strip() for l in lst][:-1]
    payload = {key: val.replace("\'",'') for key, val in
               [tag.split("=") for tag in lst]}
    payload = load_payload_detalhes(payload)
    return payload

def response_to_payload_detalhes(response: dict) -> dict:
    assert (response['iTotalRecords'] != 0)
    responses_data = response['aaData']
    lst_payload = [data_to_payload(data) for data in responses_data]
    return lst_payload


def details_to_df(key, detalhes):

    cols_pl_request = ['ano_PLR','mes_PLR','unidade_PLR','nempenho_PLR',
                   'nsubempenho_PLR','cpfcnpj_PLR','credor_PLR',
                   'modalidade_PLR','descsituacao_PLR','tipolicitacao_PLR',
                   'dataemissao_PLR', 'dthremissao_PLR', 'valorempenhado_PLR',
                   'valorliquidado_PLR', 'valorpago_PLR', 'dtultmov_PLR',
                   'valoranulado_PLR', 'estornopago_PLR', 'estornoliquidado']

    y = detalhes[0].T.copy()
    headers = y.iloc[0]
    y = pd.DataFrame(y.values[1:], columns=headers)
    #y['itens'] = False
    z = detalhes[1].copy()
    #z['_itens'] = True
    z = z[:-4]
    z['key'] = 0
    x = y.join(z.set_index('key'))
    x['itens'] = True
    y['itens'] = False
    df = y.append(x)
    df[cols_pl_request] = key
    df['numero_empenho'] = df['No Empenho:'].str[6:-3].astype('float')
    return df


def process_details(responses):
    all_details = {key: pd.read_html(data, thousands='.', decimal=',') for key, data in responses.items()}  # {[payload]: [df1, df2, df3, df4]}
    details_processed = [details_to_df(list(key), details) for key, details in all_details.items()]  # [df]
    df = pd.concat(details_processed)
    df['empenho_numero'] = df['No Empenho:'].apply(lambda x: x.split('NE')[1][:5]).astype('int')
    print(df.info)
    return df

def to_float(df, cols):
    for col in cols:
        df[col] = df[col].astype('float')
    return df

def return_subelem(l):
    return "%s" % '-'.join(l.split('-')[1:])

def objetos_empenhos(df, orgao, empenho, objeto):
    df['objetos'] = np.where(
        (df['OU_nome'] == orgao)
        & (df['empenho_numero'] == empenho),
        objeto, df['objetos'])
    return df

def grupos(df, cnpj, orgao, empenho, subel):
    for grupo, value in gdict.items():
        if (subel in value):
            df['grupo'] = np.where(
                  (df["cpf_cnpj"] == cnpj)
                & (df["OU_nome"] == orgao)
                & (df["empenho_numero"] == empenho),
                grupo, df["grupo"])
    return df





#    def get_request(self, url, payload, json=True):
#        r = requests.get(url, params=payload,
#                         headers={'Connection': 'close'},
#                         timeout=30)
#        if json:
#            return r.json()
#        else:
#            return r.text
#
#    def unidade_codigo(unidade: str) -> str:
#        pass
#    def is_unidade(unidade: str) -> bool:
#        pass
