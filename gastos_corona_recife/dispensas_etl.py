"""Script que organiza o fluxo de consultas das dispensas."""
#!/usr/bin/env python3
import pandas as pd

from gastos_corona_recife.adapters import (
    chave_dispensas_portal_transparencia,
    dispensas_portal_transparencia)
#    empenhos_portal_transparencia)

from gastos_corona_recife import (
    paths,
    transform)


def scrap_portal_dispensas():
    """ETL da página inicial do portal."""
    dict_raw = dispensas_portal_transparencia.extract()
    dfs = [transform.get_from_html_tag(dict_raw[type_req])
           for type_req in dict_raw]

    assert len(dfs) == len(dict_raw.keys())
    t_req = list(dict_raw.keys())
    [load(dfs[i], paths.PARTIAL_DISPENSA_FILES["DISP_" + t_req[i]])
     for i in range(len(dfs))]
    return dfs


def dispensas2empenho(scrap=False, consolidadas=False):
    """Chama o scrap (ou não) e devolve o df contendo #empenho."""
    if scrap:
        _dispensas = scrap_portal_dispensas()
        dispensas = _dispensas[1] if consolidadas else _dispensas[0]
        dispensas_empenho = etl_fase_2(dispensas)
    else:
        _dispensas = load_dispensas_from_file()
        dispensas = (_dispensas["DISP_CONSOLIDADO"] if consolidadas
                     else _dispensas["DISP_EXTENDIDO"])
        dispensas_empenho = _dispensas["DISP_EMPENHO"]

    dispensas_full = dispensas_empenho.join(
        dispensas.set_index("chave_lupa"),
        on="chave_lupa")

    # to load!
    # dispensas_full.to_csv(, index=False)
    load(dispensas_full, paths.FILE_DISPENSAS_RAW)

    dispensas_full = transform.dispensas_full(dispensas_full)

    load(dispensas_full, paths.FILE_DISPENSAS)

    return dispensas_full


def load_dispensas_from_file():
    """Carrega os DataFrames com dados coletados anteriormente."""
    dispensas = {_disp: pd.read_csv(paths.PARTIAL_DISPENSA_FILES[_disp])
                 for _disp in paths.PARTIAL_DISPENSA_FILES}
    assert len(dispensas) == len(paths.PARTIAL_DISPENSA_FILES)
    return dispensas


def etl_fase_2(df_dispensas=None):
    """ETL do número de empenho a partir da chave_lupa.

    A pesquisa inicial infeliz mente não revela o número
    do empenho.
    Este irá simplificar a chegada na informação
    sobre os pagamentos
    """
    if (df_dispensas is None):
        df_dispensas = pd.read_csv(
            paths.PARTIAL_DISPENSA_FILES["DISP_CONSOLIDADO"])

    chaves = df_dispensas['chave_lupa'].values
    df_lupa = pd.concat(
        [chave_dispensas_portal_transparencia.extract(chave)
         for chave in chaves])

    df_lupa.to_csv(paths.PARTIAL_DISPENSA_FILES["DISP_EMPENHO"], index=False)

    # transform_fase_2(df_lupa)
    # dfs = [transform(dict_raw[type_req]) for type_req in dict_raw]
    # assert(len(dfs) == len(dict_raw.keys()))
    # [load(dfs[i], list(dict_raw.keys())[i]) for i in range(len(dfs))]
    return df_lupa


def load(df, name):
    df.to_csv(name, index=False)
#         paths.PATH_DIR
#         + paths.DATA_DIR
#         + "dispensas_portal_transparencia_"
#         + type_req
#         + ".csv",
#         index=False)
# def load_full(df, ):
#     df.to_csv(
#         paths.PATH_DIR
#         + paths.DATA_DIR
#         + "dispensas_portal_transparencia_"
#         + type_req
#         + ".csv",
#         index=False)

# etl()
