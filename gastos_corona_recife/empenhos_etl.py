"""Módulo que irá extrair informações ligadas aos empenhos."""
#!/usr/bin/env python3
import pandas as pd

from gastos_corona_recife.adapters import (
    chave_dispensas_portal_transparencia,
    dispensas_portal_transparencia)
#    empenhos_portal_transparencia)

from gastos_corona_recife import (
    paths,
    transform)

import os
import json

import pandas as pd
from urllib import request
from datetime import datetime
from time import sleep

# from gastos_corona_recife import config, extract, transform

import gastos_corona_recife.adapters.empenhos_portal_transparencia as extract

now = datetime.now()
MONTHS = range(1,13)
YEARS = range(2020,now.year + 1)
months_years = list(filter(
    lambda a: not (a[0] > now.month and a[1] == now.year),
    [(m, y) for y in YEARS for m in MONTHS]))


def empenhos_etl():
    df_dispensas = pd.read_csv(paths.FILE_DISPENSAS)
    df_dispensas = df_dispensas[df_dispensas.ano == 2020]
    dict_consulta, cod_unidade = get_orgao_cnpj(df_dispensas)
    list_dfs_by_month = [_download_empenhos(dict_consulta, cod_unidade, *m_y)
                         for m_y in months_years]
    #dfs_by_month = pd.concat(list_dfs_by_month)
    #  dict2consulta = gen_dfs_app(df)
    return list_dfs_by_month
    

def get_orgao_cnpj(df):
    df[['cod','und','sigla']] = df["unidade"].str.split("-", expand=True)
    df['und'] = df['und'].str.strip()
    dict2consultas = {}
    for und in df['und'].unique():
        dict2consultas[und] = \
            df[df['und'] == und]['cpf/cnpj_do_fornecedor'].unique()
    df1 = df[['und','cod']]
    df1 = df1.drop_duplicates()
    df1.cod = df1.cod.str.strip()
    return dict2consultas, df1


def _download_empenhos(dict_consulta, cod_und, month, year):
    # _dict2consultas: dicionário com {ORGAO: list(CNPJs)}
    consulta = extract.Consulta(
        dict_consulta,
        mes_inicial=str(month),
        mes_final=str(month),
        year=str(year),
        tab_codigos = cod_und)
    detalhes = consulta.get_detalhes()
    #processado = _preprocess_database(detalhes)
    return detalhes
# processado


def _preprocess_database( detalhes):
    print('PREPROCESSAMENTO')
    print(type(detalhes))
    return transform.detalhes2df_coronagastos(detalhes)



def get_df_dispensas(self):
    return _cached_df_dispensas.copy()

def get_df_empenhos(self):
    return _cached_df_empenhos.copy()

def    _gen_dfs_app(self):
    '''
       retorna dicionario contendo todos os dataFrames necessarios para a aplicação/página
       '''
    pass

def df2csv(self):
    _cached_df_dispensas.to_csv(config.source_dispensas)
    _cached_df_empenhos.to_csv(config.source_empenhos)
    _ts_download('df2csv', 'Exportado')

def _partial_df2csv(total=False):
    dfs = []
    for x in _partial_df.keys():
        dfs.append(_partial_df[x])
        df = pd.concat(dfs, ignore_index=True)
    arquivo = 'parcial_dfs_{}'.format(''.join(list(
        _partial_df.keys())))
    if (not total):
        df.to_csv('data/' + arquivo + '.csv')
    else:
        _cached_df_empenhos = df.copy()
        df2csv()

def _partial_request(inicio, now):
    for month in range(inicio, now.month + 1):
        print('\n---> Download de empenhos relativos ao mês {}\n'.format(month))
        try:
            processado = _download_empenhos(str(month))
            print( f"tentando mês {month}")
        except:
            print( f"distentando mês {month}")
            ### salva oq tem
            _partial_df2csv()
            sleep(30)
            ### re tenta a partir do mês parado
            update_databases(mes=month)
            _partial_df[str(month)] = processado
