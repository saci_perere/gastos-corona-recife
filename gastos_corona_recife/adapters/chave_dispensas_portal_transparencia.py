"""
Extrator a partir das chaves das dispensas.

Módulo destinado a implementar a extração dos dados
que ficam ESCONDIDOS dentro da chave no resultado
da busca por dispensas de licitação no portal
da transparência
"""
#!/usr/bin/env python3

import pandas as pd
import requests
import tqdm

import gastos_corona_recife.config as config

requests.adapters.DEFAULT_RETRIES = 30

URL_DETALHES = 'http://transparencia.recife.pe.gov.br/codigos/web/covid19/despesaContratacoesAquisicoes_detalhe_ajax.php'


def extract(chave):
    """Método extrator."""
    # df = pd.concat([get_url(chave)for chave in chaves])
    df = get_url(chave)
    return df


def get_url(chave):
    """Método que faz o request e devolve df."""
    r = requests.get(
        URL_DETALHES + '?chave=' + chave,
        headers={'Connection': 'close'},
        timeout=30)
    list_df = pd.read_html(r.text)
    df0 = list_df[0].T
    headers = df0.iloc[0]
    df0 = pd.DataFrame(df0.values[1:], columns=headers)
    df0['id'] = df0.index
    # Retirar a segunda tabela (indice 1),
    # pode ser necessaria posteriormente:
    # com link para o processo na integra (PDF)
    # list_df = [df0, *list_df[1:]]
    df2 =  list_df[2]
    list_df = [df0, df2]
    for _df in list_df:
        _df['chave_lupa'] = chave

    assert (len(df0) == 1)
    df = df2.join(df0.set_index('chave_lupa'), on='chave_lupa')

    return df
