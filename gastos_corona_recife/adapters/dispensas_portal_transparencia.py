#!/usr/bin/env python3

import pandas as pd
import requests

import gastos_corona_recife.config as config

ANOS = [2020, 2021]

URL_EXTENDIDO = "http://transparencia.recife.pe.gov.br/codigos/web/covid19/despesaContratacoesAquisicoes_extendido_resultado_ajax.php?ano="
URL_CONSOLIDADO = "http://transparencia.recife.pe.gov.br/codigos/web/covid19/despesaContratacoesAquisicoes_resultado_ajax.php?ano="

COLS_NOT_CONSOLIDADO = [
    'descricao_item', 'qtd_item', 'valor_unitario_item', 'valor_total_item']

COLS_EXTENDIDO = [
    'chave_lupa', 'modalidade', 'situacao', 'num_processo', 'unidade',
    'objeto', 'data_vigencia', 'valor_total_processo', 'descricao_item',
    'qtd_item', 'valor_unitario_item', 'valor_total_item',
    'valor_fornecedor', 'nome_fornecedor', 'cpf_cpnj_fornecedor',
    'num_contrato']

COLS_CONSOLIDADO = list(filter(lambda x:
                               x not in COLS_NOT_CONSOLIDADO, COLS_EXTENDIDO))

TYPES_REQUEST = {
    "EXTENDIDO": {"URL": URL_EXTENDIDO,
                  "COLS": COLS_EXTENDIDO},
    "CONSOLIDADO": {"URL": URL_CONSOLIDADO,
                    "COLS": COLS_CONSOLIDADO}
}

PATH = config.source_dispensas


def extract():
    """Método extrator."""
    return {type_request:
            pd.concat([get_url(type_request, ano) for ano in ANOS])
            for type_request in TYPES_REQUEST}


def get_url(type_request, ano):
    """Método que faz o request e devolve df."""
    url = TYPES_REQUEST[type_request]["URL"] + str(ano)
    r = requests.get(url)
    df = pd.DataFrame(r.json()['aaData'],
                      columns=TYPES_REQUEST[type_request]["COLS"])
    df['ano'] = ano
    df['type_request'] = type_request
    return df
