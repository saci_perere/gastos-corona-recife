#!/bin/python

"""
Para cada órgão retorna uma lista, cada elemento referente a um CNPJ,
contendo listas com dois dataframes, referente às tabelas horizontais e
verticais encontradas dentro do detalhamento de cada linha da resposta
à query utilizando payload_consulta
Admite entrada enquanto órgão ou unidade. Separa os dois casos dentro da
inicialização da classe, baseado no dicionário contido no módulo paths.
<TODO>
- Receber qualquer query (não apenas cnpj * orgao)
- Validar entradas (utilizando dicionário em paths)
"""

import sys
import requests
import urllib
from time import sleep
import pandas as pd
from bs4 import BeautifulSoup
# from datetime import datetime

from gastos_corona_recife import config
# from gastos_corona_recife.scripts.consultas import Consults_details

requests.adapters.DEFAULT_RETRIES = 20


class Consulta:
    """Armazena e atualiza resultados de busca no Portal da Transparência.
    ---
    Input: Dicionário {órgãos: [cnpjs]}
    Atributos:
    - consultas
    - detalhes
    Métodos:
    - get_consultas()
    - get_detalhes()
    """

    def __init__(
            self, dict_orgao_cnpj,
            mes_inicial='1', mes_final='12',
            year='2020', tab_codigos=None):
        self.dict_payload_consulta = {}
        self.payload_details = {}
        self.consulta2detalhes = []
        self.consultas = pd.DataFrame()
        # [[df1,df2],[df1,df2],...]
        self.lista_detalhes = []
        print(dict_orgao_cnpj)
        for orgao in dict_orgao_cnpj.keys():
            print('----')
            print(orgao)
            try:
                codigo = config.nome_codigo[orgao.upper()]
            except:
                codigo = input('codigo')
            hierarquia = 'unidade' if ('.' in codigo) else 'orgao'

            self.dict_payload_consulta[orgao] = [
                    {'ano': year,
                     'mesInicial': mes_inicial,
                     'mesFinal': mes_final,
                     'cnpj': cnpj,
                     hierarquia:
                     tab_codigos[tab_codigos['und_comp'] == orgao] ['cod'].values[0]
                     } for cnpj in dict_orgao_cnpj[orgao]
            ]


    def _load_payload_detalhes(self, a):
        self._payload_detalhes = {
            'ano': a['ano'],
            'mes': a['mes'],
            'unidade': a['unidade'],
            'nempenho': a['empenho'],
            'nsubempenho': a['subempenho'],
            'cpfcnpj': a['id'],
            'credor': a['nome'],
            'modalidade': a['modalidade'],
            'descsituacao': a['desc_situacao'],
            'tipolicitacao': a['tipo_licitacao'],
            'dataemissao':a['dataemissao'],
            'dthremissao': a['dthremissao'],
            'valorempenhado': a['valor_empenhado'],
            'valorliquidado': a['valor_liquidado'],
            'valorpago': a['valor_pago'],
            'dtultmov': a['dtultmov'],
            'valoranulado': a['valor_anulado'],
            'estornopago': a['estorno_pago'],
            'estornoliquidado': a['estorno_liquidado'],
        }

    def _get_request(self, url, payload):
        return requests.get(url, params=payload,
                            headers={'Connection': 'close'},
                            timeout=30)

    def _consulta(self, payload):
        url = config.portal_transparencia
#        import pdb; pdb.set_trace()
        r = self._get_request(url, payload)
        return BeautifulSoup(r.text, features='lxml').find_all('a')

    def get_consultas(self):
        print(':::Consultas:::')
        for orgao in self.dict_payload_consulta.keys():
            print('::::::{}::::::'.format(orgao))
            for payload in self.dict_payload_consulta[orgao]:
                for linha in self._consulta(payload):
                    import pdb; pdb.set_trace()
                    #print(linha)
                    linha['orgao'] = orgao
                    self.consulta2detalhes.append(linha.attrs)
                    result = self.consultas.append(
                        pd.DataFrame.from_dict(
                            linha.attrs, orient='index').T
                    )
        return self.consultas

    def _detalhe(self, payload):
        url = config.portal_transparencia_detalhes
        r = self._get_request(url, payload)
        return BeautifulSoup(r.text, features='lxml')

    def get_detalhes(self):
        print('!')
        print('{}'.format(self.dict_payload_consulta))
        print('!')
        lista = []
        if (self.consulta2detalhes == []):
            self.get_consultas()
        print(':::Detalhamentos:::')
        for linha in self.consulta2detalhes:
            print(linha)
            orgao = linha['orgao']
            print('Órgão: {}\tCNPJ:{}\n'.format(orgao,linha['id']))
            self._load_payload_detalhes(linha)
            resultado = self._detalhe(self._payload_detalhes)
            resultado = pd.read_html(str(resultado))
            #print (type(resultado[0]))
            #print (len(resultado))
            lista.append({orgao: resultado})
            sleep(2)
        return lista

    def df2csv(self):
        pass

    def csv2df(self):
        pass

class Dispensas:

    _cached_df_dispensas = None

    def __init__(self):
        if (Dispensas._cached_df_dispensas == None):
            self._download_dispensas()

    def _link2csv(self, csv):
        with open (config.source_dispensas, 'w') as f:
            f.write(csv)

    def _download_dispensas(self):
        url = config.portal_transparencia_dispensas
        r = requests.get(url)
        y = BeautifulSoup(r.text, features='lxml')
        for z in y.find_all('a'):
            if ('href' in z.attrs):
                link = z.attrs['href']
                if  (('CONSOLIDADO' in link) & ('csv' in link)):
                    break
        link = urllib.request.pathname2url(link[7:])
        link = 'http://' + link
        r = requests.get(link)
        self._link2csv(r.text)
        self._cached_df_dispensas = pd.read_csv(config.source_dispensas, delimiter=';')

    def get_dispensa(self):
        type(self._cached_df_dispensas.copy())
        return self._cached_df_dispensas.copy()


# # def main(dict_orgao_cnpj):
# def main(cnpj_orgao):
#     pl = Consults_details(cnpj_orgao[0], cnpj_orgao[1])
#     consulta_table = pl.get_consulta()
# #    return consulta_table
#     dfs = []
#     for x in consulta_table:
#         pl.load_details(x.attrs)
#         y = pl.get_details()
#         ### y contém duas tabelas: uma horizontal e outra vertical
#         dfs.append(pd.read_html(str(y)))
#     return dfs
#
# if (__name__ == '__main__'):
#     main(sys.argv[1:])
