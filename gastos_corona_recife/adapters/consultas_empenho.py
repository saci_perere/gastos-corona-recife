#!/usr/bin/env ipython
#!/bin/python

import sys
import requests
import urllib
from time import sleep
import pandas as pd
from bs4 import BeautifulSoup
# from datetime import datetime

from gastos_corona_recife import config
# from gastos_corona_recife.scripts.consultas import Consults_details

MONTHS = range(1,13)
YEARS = range(2020,datetime.now().year + 1)

class Consulta:
    """Armazena e atualiza resultados de busca no Portal da Transparência.
    ---
    Input: Dicionário {órgãos: [cnpjs]}
    Atributos:
    - consultas
    - detalhes
    Métodos:
    - get_consultas()
    - get_detalhes()
    """
    self.url = "http://transparencia.recife.pe.gov.br/codigos/web/despesas/despesaDetalhadaCredorServerProcessing.php"
    self.all_payloads = {}
    self.df_consulta =  pd.DataFrame()

    self.months_years = list(filter(
            lambda a: not (a[0] > now.month and a[1] == now.year),
            [(m, y) for y in YEARS for m in MONTHS]))

    def __init__(self, unidade_cnpj, tab_codigos=None):
        for unidade in unidade_cnpj:
            codigo = utils.unidade_codigo(unidade)
            unidade = 'unidade' if utils.is_unidade(unidade) else 'orgao'
            self.all_payloads[unidade] = [{'cnpj': cnpj, unidade: codigo}
                                     for cnpj in unidade_cnpj[unidade]]

    def get_consultas(self):
        """Scrap de todos os payloads."""
        for unidade in self.all_payloads:
            for all_payloads in self.all_payloads[unidade]:
                df = json_to_dataframe(json_consulta)
                df['unidade'] = unidade
            self.df_consulta.append(df)
        return self.df_consulta

    def json_to_dataframe(j: dict) -> pd.DataFrame:
        """Transforma as respostas do scrap em dataframe."""
        pass

    def set_payloads(payloads: dict):
        """Seta os payloads para realizar requests sob demanda."""
        self.all_payloads = payloads
