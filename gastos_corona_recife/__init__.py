"""Package: Gastos Corona Recife
Módulos que realizam a extração de dados do Portal da Transparência da cidade do Recife, transforma-os e salva em formato csv.
Queremos disponibilizar dados dispersos por interfaces diferentes e de difícil agrupamento de uma forma simples e legível por máquina.
Adicionalmente realizamos uma série de transformações necessárias para a incorporação dos dados na página: gastoscovid.recife.br
"""
