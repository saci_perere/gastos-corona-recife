# Gastos Corona Recife

### Scrap e unificação dos dados de despesas da prefeitura do Recife com o coronavírus.

Passo 0: Todos os passos a seguir admitem que estamos usando o Python3/pip3 como padrão do SO, caso não seja o seu caso ajustar o comando.

Passo 1: Ativar o ambiente virtual:

``` shell
python -m venv venv
. venv/bin/activate
```

Passo 2: Instalar as dependências e o pacote:

``` shell
pip install -r requirements.txt 
python3 setup.py install
```

Passo 3:

``` shell
python -m gastos_corona_recife
```




Antes de rodar é necessário atualizar dois arquivos (pendente de
automação):

Baixar a lista com todos os CNPJs dispensados de licitação (servirá
para não realizarmos uma busca em todos os CNPJs do Portal da
Transparência.

[Nesta
página](http://transparencia.recife.pe.gov.br/codigos/web/covid19/despesaContratacoesAquisicoes.php)
você poderá realizar uma pesquisa apenas com o parâmetro _Ano = 2020_

Você poderá salvar a resposta em formato CSV. Clicar em _salvar como_
logo abaixo os gráfico (lado direito da tela) e salvar com o nome
_dispensas.csv_ dentro da pasta
_gastos-corona-recife/gastos\_corona\_recife/data_

Como a Prefeitura segue sem indicar o campo _referência legal_ no
Portal da Transparência é necessário ter uma lista auxíliar com todos
os empenhos (eles poderiam ser a fonte inicial da busca, entretanto só
podemos pegar esse dado recentemente e não deu ainda pata atualizar o
scrap).

Este arquivo é gerado quando rodamos o Jupyter Notebook que está em
_notebooks/new-scrap.ipynb_

Agora sim podemos rodar e esperar os dados o mais correto
possíveis. Ainda há diferenças entre o resultado da nova visualização
dos gastos no Portal, os raspados pelo _new-scrap.ipynb_ e o resultado
final deste módulo. Acreditamos que seja decorrente de defasagens
internas do próprio Portal.

Rodar (dentro da pasta do repositório):

```
python3 -m gastos_corona_recife
```


Ao final haverá dois arquivos na pasta *data*:

```
data/dispensas.csv
data/empenhos.csv
```

Dentro do ambiente virtual é também possivel importar a classe, obter as planilhas no formato Pandas DataFrames:

```
import gastos_corona_recife as gcr

data = gcr.Data()

df_dispensas = data.get_df_dispensas()
df_empenhos = data.get_df_empenhos()
```


O código é capaz de replicar a base de dados da página [Gastos COVID-19 Recife](gastoscovid.recife.br).

Se quer ajudar em qualquer desses corres para melhorar essa raspagem e disponibilização dos dados é só chegar ;)

Para mais informações sobre a ausência de transparência da prefeitura do Recife e a motivação deste trabalho é só consultar a [Nota Técnica Nº 05 - A difícil tarefa de encontrar, entender e garantir a totalidade dos dados no Portal da Transparência da Prefeitura do Recife](https://uploads.strikinglycdn.com/files/f666509d-3b64-4b9c-bfef-9116db87b213/Nota%20T%C3%A9cnica%20N%C2%BA%2005%20Covid%20PCR.pdf) construída pelo mandato do vereador da cidade do Recife Ivan Moraes (PSOL).
