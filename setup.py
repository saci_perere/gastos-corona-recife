from pathlib import Path

from setuptools import find_packages, setup

setup(
    author="Saci Pererê",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Topic :: Utilities",
    ],
    description="Scrap do Portal da Transparência do Recife para gastos com COVID-19 via dispensa de licitação",
    keywords="Recife, COVID, dispensa, licitação",
    license="GPLv3",
    name="gastos-corona-recife",
    packages=find_packages(),
    package_data = {'gastos_corona_recife': ['data/*.csv', 'logs/*.txt']},
    py_modules=["gastos_covid_recife"],
    url="",
    version="0.1",
    zip_safe=False,
    entry_point="""
    [console_scripts]
    update = gastos_corona_recife.data:update_databases
"""
)
