import unittest
import pandas as pd

from gastos_corona_recife import Data
from gastos_corona_recife import extract

cnpj = '27.058.274/0001-98'
orgao = 'FUNDO MUNICIPAL DOS DIREITOS DA PESSOA IDOSA'

class Tdd_corona(unittest.TestCase):

    def setUp(self):
        self.data = Data()
        self.consulta = extract.Consulta({orgao:[cnpj]})

    def test_get_df_dispensas(self):
        result = self.data.get_df_dispensas()
        self.assertIsInstance(result, pd.DataFrame)

    def test_get_df_empenhos(self):
        result = self.data.get_df_empenhos()
        self.assertIsInstance(result, pd.DataFrame)

    def test_consulta(self):
        result = self.consulta.get_consultas()
        self.assertIsInstance(result,pd.DataFrame)

    def test_detalhes(self):
        result = self.consulta.get_detalhes()
        self.assertIsInstance(result, list)
        for x in result:
            self.assertIsInstance(x, dict)
            for y in x.keys():
                self.assertEqual(len(x[y]), 2)
                for z in x[y]:
                    self.assertIsInstance(z, pd.DataFrame)

    def test_preprocessamento(self):
        resultado = self.data.update_databases()
        self.assertIsInstance(resultado, pd.DataFrame)
