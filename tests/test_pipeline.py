#!/usr/bin/env python3

import unittest
import pandas as pd
#from pandas._testing import assert_frame_equal
#from gastos_corona_recife import dispensas_etl
from gastos_corona_recife import transform

import pathlib

PATH_DIR = str(pathlib.Path(__file__).parent.parent)

df_consolidado = pd.read_csv(
    PATH_DIR
    + "/gastos_corona_recife/data/dispensas_portal_transparencia_CONSOLIDADO.csv")

class Test_Pipeline(unittest.TestCase):
    def test_num_cols_consolidado(self):
        assert(df_consolidado, 14)


if __name__ == '__main__':
    unittest.main()
