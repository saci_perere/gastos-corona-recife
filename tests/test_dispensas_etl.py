import unittest
import pandas as pd
from pandas._testing import assert_frame_equal
import numpy as np
from gastos_corona_recife import dispensas_etl, schemas, transform

df1 = transform.tmp(pd.read_csv(
    'gastos_corona_recife/data/tests/dispensas_test.csv'))
df2 = pd.read_csv(
    'gastos_corona_recife/data/tests/dispensas_test_2.csv')


class Test_Dispensas(unittest.TestCase):

    # def test_old_CSV_equal_trasformed_from_raw(self):
    #    # Como comparar dois dataframes
    #    self.assertTrue(assert_frame_equal(df1, df2),
    #                    "Os dataframes não são idênticos!")

    def test_types_int(self):
        for col in schemas.INT_COLS:
            self.assertTrue(
                pd.api.types.is_int64_dtype(df1[col]),
                f"Coluna {col.upper()} deveria ser int64!"
            )

    def test_types_date(self):
        for col in schemas.DATE_COLS:
            self.assertTrue(
                pd.api.types.is_datetime64_ns_dtype(df1[col]),
                f"Coluna {col.upper()} deveria se datetimae[ns]"
            )

    def test_types_float(self):
        for col in schemas.FLOAT_COLS:
            self.assertTrue(
                pd.api.types.is_float_dtype(df1[col]),
                f"Coluna {col.upper()} deveria ser float!"
            )

    def test_full_cols(self):
        pass

    def test_suffix_repeated_cols(self):
        df0 = pd.DataFrame([1,2,3,4,5,0]).T
        df0.columns = ['a', 'b', 'a','c','d','b']

        df1 = pd.DataFrame({'a_0': [1],
                            'b_0': [2],
                            'a_1': [3],
                            'c': [4],
                            'd': [5],
                            'b_1': [0]
                            })

        df2 = transform._suffix_in_repeated_cols(df0)

        assert(df1.columns.equals(df2.columns))

    def test_transform_dispensas_full_normalize_str(self):
        df0 = pd.DataFrame({'Ação': [1],
                            'Café': [2],
                            'Caféx': [np.nan],
                            'cacaU': [4],
                            'dói': [5],
                            'BJO': [np.nan]
                            })

        df1 = pd.DataFrame({'acao': [1],
                            'cafe': [2],
                            'cacau': [4],
                            'doi': [5],
                            })

        df2 = transform.dispensas_full(df0)

        assert_frame_equal(df1, df2)


if __name__ == '__main__':
    unittest.main()
